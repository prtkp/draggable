import "./App.css"
import React, {useState} from 'react';
import Draggable from "react-draggable";
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

function App() {

  const [currNumber, setCurrNumber] = useState(1); // Update Click-Box number
  const [currWidth, setCurrWidth] = useState(100); // Current Click-Box width

  const [draggrableComponentProps, setDraggableComponentProps] = useState([
    { right: null, bottom: null, width: "100px", height: "100px", num: 1 }
  ]);

  /* draggableComponent */
  let draggableComponent = draggrableComponentProps.reduce((child, curr) => {
    return (
      <Draggable
        onStart={(e, data) => e.stopPropagation()} // for Nested Draggable (picked from github issue: https://github.com/react-grid-layout/react-draggable/issues/11#issuecomment-112642622)
        bounds={{ left: 0, top: 0, right: curr.right, bottom: curr.bottom }}>
        <div className="clickBox"
             style={{ width: curr.width,
             height: curr.height }}>
          <b>Click {curr.num}</b>
          {child}
        </div>
      </Draggable>
    );
  }, <></>);
    
  /* Handler function invoked on every Click */
  const newComponentHandler = () => {
    let newWidth = currWidth + 100;
    setCurrWidth(newWidth);

    let newDraggableComponentProps = [
      ...draggrableComponentProps,
      {
        right: null,
        bottom: null,
        width: newWidth + "px",   // Update the width
        height: newWidth + "px",  // Update the height w.r.t width
        num: currNumber + 1       // Increase the number by 1 for every new box
      }
    ];
    
    /* Draggable limit breadthwise in a Box */
    newDraggableComponentProps[newDraggableComponentProps.length - 2].right = 100;
    
    /* Draggable limit from bottom in a Box */
    newDraggableComponentProps[newDraggableComponentProps.length - 2].bottom = 82;

    /* Increment the currNum in every Click */
    setCurrNumber(currNumber + 1);

    /* Update the draggable Props */
    setDraggableComponentProps(newDraggableComponentProps);
  };

  return (
  <>
    {/* Insert "Add Parent" Button */}
    <Button variant="contained"
           endIcon={<AddIcon />}
           sx={{m:1, mx:"44%", w:"100%" }}
           onClick={newComponentHandler}>
      Add Parent
    </Button>
    
    {/* Insert draggableComponent */}
    {draggableComponent}
  </>
  );
}

export default App;