# Draggable

## Issues Faced in Implementation 😐

* Used Card Component of React Bootstrap (should have used a Box)
* Card Component made nested dragging difficult.
* Implementation of Nested draggable ([github issue helped](https://github.com/react-grid-layout/react-draggable/issues/11#issuecomment-112642622))


## Run Locally ✨

Install the dependencies and devDependencies and start the server.

```sh
cd draggable
npm i
npm start
```
